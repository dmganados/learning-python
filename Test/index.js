console.log('Hello World')
// function pokemon(name, lvl, hp){
// 	this.name = name
// 	this.level = lvl;
// 	this.health = hp;
// 	this.attack = lvl;
// 	this.tackle = function(target){
// 		console.log(this.name + 'tackled ' + target.name)	
// }

// let jigglypuff = new pokemon ('Jigglypuff ', 5, 15);
// let snorlax = new pokemon ('Snorlax ', 4, 15);

// jigglypuff.tackle(snorlax);
// snorlax.tackle(jigglypuff);

// // console.log(new pokemon[1])

// let sentence = ['The', 'brain', 'is', 'loading']

// let reducedSentence = sentence.reduce(function(x, y){
// 	return x + " " + y
// });
// console.log('Result from reduce method:' + reducedSentence)

let title = 'The Lord of the Rings';
console.log(title[2]);

// for (let i = 0; 1 < 10; ++i) {
// 	console.log(i)
// }

let conditionA = false;
let conditionB = true;
let conditionC = false;
let result = !(conditionA && (conditionB || conditionC));
console.log(result);

for (let i = 1; i < 5; i++) {
	console.log(i * i)
}

let students = ['John', 'Paul', 'George', 'Ringo'];
console.log('Here are the graduating students:')
for (let count = 0; count <= students.length; count++) {
	console.log(students[count]);
}

function check(dividend, divisor) {
	if (dividend % divisor == 0) {
		console.log(`${dividend} is divisible by ${divisor}`)
	} else {
		console.log(`${dividend} is not divisible by ${divisor}`)
	}
}

check(100, 0)

let items = [
	{
		id: 1,
		name: 'Banana',
		description: 'A yellow fruit',
		price: 15.00,
		category: 2,

	},
	{
		id: 2,
		name: 'Pork Cutlet',
		description: 'Japanese Kurobuta',
		price: 190.00,
		category: 1,

	},
	{
		id: 1,
		name: 'Sweet Potato',
		description: 'Best when roasted',
		price: 20.00,
		category: 3,

	}
];

// console.log(items.length)

for (let i = 0; i < items.length; i++) {
	console.log(`
	Name: ${items[i].name}
	Description: ${items[i].description}
	Price: ${items[i].price}
	`);
}

for (let row = 1; row < 3; row++) {
	for (let col = 1; col <= row; col++) {
		console.log(`Current row: ${row}, Current col: ${col}`)
	}
}

console.log('first');
setTimeout(function() {
	console.log("second");
}, 0)
console.log("third")

// This is a jquery lesson
// ready is used to prevent any jQuery code from running before the document are finished loading
$(document).ready(function(){
	$("#pbutton").click(function(){
		// when button is clicked the class="par1" in HTML file will be hidden, and the id="secondPar" will change the color to red
		$(".par1").hide();
		$("#secondPar").css("color", "red")
	});
});

// click()
// When a click event fires on a .click element; hide the current .click element
$(document).ready(function(){
	$(".click").click(function(){
		$(this).hide();
	});
});

// dblclick()
// The function is executed when user double-clicks the HTML element
$(document).ready(function(){
	$('.double').dblclick(function(){
		$(this).hide();
	});
});

// mouseenter()
$(document).ready(function(){
	$("#p1").mouseenter(function(){
		alert('You entered p1!')
	});
});

// mouseleave()
$(document).ready(function(){
	$('#p2').mouseleave(function(){
		alert('Bye! You now leave p2!');
	});
});

// mousedown()
$(document).ready(function(){
	$("#p3").mousedown(function(){
		alert("Mouse down over p1!");
	});
});

// mouseup()
$(document).ready(function(){
	$("#p4").mouseup(function(){
		alert("Mouse up over p4!");
	});
});

// hover()
$(document).ready(function(){
	$("#p5").hover(function(){
		alert("You entered p5!");
	},
	function(){
		alert("Bye! You now leave p5!")
	});
});

// focus() & blur()
$(document).ready(function(){
	$("input").focus(function(){
		$(this).css("background-color", "yellow");
	});
	$("input").blur(function(){
		$(this).css("background-color", "green");
	});
});

// on()
$(document).ready(function(){
	$(".p6").on("click", function(){
		$(this).hide();
	});
});

$(document).ready(function(){
	$(".p7").on({
		mouseenter: function(){
			$(this).css('background-color', "lightgray");
		},
		mouseleave: function(){
			$(this).css("background-color", "lightblue");
		},
		click: function(){
			$(this).css('background-color', "yellow");
		}
	});
});

// hide() and show()
$(document).ready(function(){
	$("#hide").click(function(){
		$('#p8').hide();
	});
	$("#show").click(function(){
		$('#p8').show();
	});
});

// using the millisecond on speed option
$(document).ready(function(){
	$("#p9button").click(function(){
		$(".p9").hide(1000);
	});
});

// toggle()
$(document).ready(function(){
	$("#p10button").click(function(){
		$(".p10").toggle();
	});
});

// fadeIn()
$(document).ready(function(){
	$("#p11button").click(function(){
		$("#div1").fadeIn();
		$("#div2").fadeIn("slow");
		$("#div3").fadeIn(3000);
	});
});

// fadeOut()
$(document).ready(function(){
	$("#p12button").click(function(){
		$("#div4").fadeOut();
		$("#div5").fadeOut("slow");
		$("#div6").fadeOut(3000);
	});
});

// fadeToggle()
$(document).ready(function(){
	$("#p13button").click(function(){
		$('#div7').fadeToggle();
		$('#div8').fadeToggle("slow");
		$('#div9').fadeToggle(3000);
	});
});

// fadeTo()
$(document).ready(function(){
	$("#p14button").click(function(){
		$("#div10").fadeTo("slow", 0.15);
		$("#div11").fadeTo("slow", 0.4);
		$("#div12").fadeTo("slow", 0.7);
	});
});

// slideDown()
$(document).ready(function(){
	$("#flip").click(function(){
		$("#panel").slideDown("slow");
	});
});

// slideUp()
$(document).ready(function(){
	$("#flip2").click(function(){
		$("#panel2").slideUp("slow");
	});
});

// slideToggle()
$(document).ready(function(){
	$("#flip3").click(function(){
		$("#panel3").slideToggle("slow");
	});
});

// animation()
$(document).ready(function(){
	$("#p15button").click(function(){
		$("#div15").animate({
			// We will manipulate multiple properties here for better animation
			// left: '250px',
			// opacity: '0.5',
			// height: '150px',
			// width: '150px'

			// We will use relative values on this part
			// left: '250px',
			// height: '+=150px',
			// width: '+=150px'

			// This is a pre-defined values ("show", "hide", or "toggle")
			height: 'toggle'
		});
	});
});

// jQuery animate() - Uses Queue Functionality
$(document).ready(function(){
	$("#p16button").click(function(){
		let container = $("#div16");
		// container.animate({height: '300px', opacity: '0.4'}, "slow");
		// container.animate({width: '300px',  opacity: '0.8'}, "slow");
		// container.animate({height: '100px', opacity: '0.4'}, "slow");
		// container.animate({width: '100px', opacity: '0.8'}, "slow");

		// Here we will move the <div> element to the right, and then increase the font size of the text
		container.animate({left: '100px'}, "slow");
		container.animate({fontSize: '3em'}, "slow");
	});
});

// stop()
$(document).ready(function(){
	$("#flip4").click(function(){
		$("#panel4").slideDown(5000);
	});
	$("#stop").click(function(){
		$("#panel4").stop();
	});
});

// callback function
$(document).ready(function(){
	$("#callback").click(function(){
		$("#p17").hide("slow", function(){
			alert("The paragraph is now hidden");
		});
	});
});

// no callback parameter
$(document).ready(function(){
	$("#noCallback").click(function(){
		$("#p18").hide(1000);
		alert("The paragraph is now hidden");
	});
});

// chaining
$(document).ready(function(){
	$("#p19Button").click(function(){
		$("#p19").css("color", "red")
		.slideUp(2000)
		.slideDown(2000);
	});
});

// DOM Manipulation
// text() and html()
$(document).ready(function(){
	$("#btn1").click(function(){
		alert("Text: " + $("#test").text());
	});
	$("#btn2").click(function(){
		alert("HTML: " + $("#test").html());
	});
});

// val()
$(document).ready(function(){
	$("#buttonVal").click(function(){
		alert("Value: " + $("#test2").val());
	});
});

// attr()
$(document).ready(function(){
	$("#buttonAttr").click(function(){
		alert($("#fb").attr("href"));
	});
});

// Set Content and Attributes
$(document).ready(function(){
	$("#btn3").click(function(){
		$("#test3").text("Hello World!");
	});
	$("#btn4").click(function(){
		$("#test4").html("<b>Hello world!</b>");
	});
	$("#btn5").click(function(){
		$("#test5").val("Dolly Duck");
	});
});

// Callback function
$(document).ready(function(){
	$("#btn6").click(function(){
		$("#test6").text(function(i, origText){
			return "Old text: " + origText + "New text: Hello World! (index: " + i + ")";
		})
	})
	$("#btn7").click(function(){
		$("#test7").html(function(i, origText){
			return "Old html: " + origText + "New html: Hello <b>world!</b> (index: " + i + ")";
		});
	});
});

// Set attr()
$(document).ready(function(){
	$("#btn8").click(function(){
		// $("#fbLink").attr("href", "https://twitter.com/home");
		// Or it could be
		$("#fbLink").attr({
			"href": "https://twitter.com/home",
			"title": "W3Schools jQuery Tutorial"
		});
	});
});

// Callback function for attr()
$(document).ready(function(){
	$("#btn9").click(function(){
		$("#twitter").attr("href", function(i, origValue){
			return origValue + "/dmganados/";
		});
	});
});

// jQuery - Add Elements
// append()
$(document).ready(function(){
	$("#btn10").click(function(){
		$(".p20").append(" <b>Appended text</b>.");
	});

	$("#btn11").click(function(){
		$("ol").append("<li>Appended item</li>");
	});
});

// prepend()
$(document).ready(function(){
	$("#btn12").click(function(){
		$(".p22").prepend("<b>Prepend text</b>. ");
	});

	$("#btn13").click(function(){
		$("#ol2").prepend("<li>Prepend item</li>");
	});
});

// adding several new elements with append() and prepend()
function appendText() {
	let txt1 = "<p>Text.</p>"; // Create text with HTML
	let txt2 = $("<p></p>").text("Text."); // Create text with jQuery
	let txt3 = document.createElement("p");
	txt3.innerHTML = "Text."; // Create text with DOM
	$("body").append(txt1, txt2, txt3); // Append new elemetns
}

// after() and before() methods
$(document).ready(function(){
	$("#btn14").click(function(){
		$("#img1").before("<b>Before</b>");
	});

	$("#btn15").click(function(){
		$("#img1").after("<i>After</i>");
	});
});

// adding severeal new elements with after() and before()
function afterText() {
	let txt4 = "<b>I </b>";
	let txt5 = $("<i></i>").text("love ");
	let txt6 = document.createElement("b");
	txt6.innerHTML = "jQuery";
	$("#img2").after(txt4, txt5, txt6);
}

// remove()
$(document).ready(function(){
	$("#btn16").click(function(){
		$("#div17").remove();
	});
});

// empty()
$(document).ready(function(){
	$("#btn17").click(function(){
		$("#div18").empty();
	});
});