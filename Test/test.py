# print("Hello World")
# print(1000-125)
# Drawing using python
# print("   /|")
# print("  / |")
# print(" /  |")
# print("/___|")

# Variable and Data Types
# character_name = "John"
# character_age = "50"
# print("There was once a man named " + character_name + ", ")
# print("he was " + character_age + "years old. ")

# character_name = "Mike"
# print("He really liked the name " + character_name + ", ")
# print("but didn't like being " + character_age + ".")

# Data types
# strings are just plain text --> ex. character_name = "Tom"
# numbers don't need a quotation mark --> ex. quantity = 50.2
# boolean value is a True or False data --> ex. is_Male = True

# Working with Strings
print("Girrafe\nAcademy") # to create a new line into the string
print("Giraffe\"Academy") # to print out the quotation mark
# \ is called escape character
phrase = "Giraffe Academy" # using a variable
print(phrase + " is cool") # concatenation
# function - a block of code that can perform specific operation
print(phrase.lower())
print(phrase.upper())
print(phrase.isupper()) # the result depends ont he the given value
print(phrase.upper().isupper()) # the result is True because at first it was changed to upper()
print(len(phrase)) # gives the length of the string

print(phrase[3]) # specify the index of the character that you want to get
# The first index starts with 0 ex. 0123456...14 = Giraffe Academy
print(phrase.index("a")) # passing a parameter. Tells you the exact order of the character in your string.
print(phrase.replace("Giraffe", "Elephant")) # replace the value of the given variable. 

# Working with Numbers
print(2)
print(2.0535)
print(-3.54)
# Arethmetic
print(3 * (4 + 5))
print(10 % 3) # modulus operator - gives the value of the remainder

my_num = 5
print(my_num)
print(str(my_num) + " is my favorite number") # converting the number to string


new_num = -7
print(abs(new_num)) # abs - absolute value
print(pow(3, 2)) # gives the number and the power ex. 3^2
print(max(4, 6 )) # tells which number is higher
print(min(4, 6)) # tells which number is the smallest
print(round(3.56)) # round the numbers

from math import * # to access more math functions from external codes.This is called module

print(floor(3.7)) # grab the lowest number. It will disregard the decimal
print(ceil(3.2)) # rounds up the number no matter what the value on the decimal
print(sqrt(36)) # gives the square root of the number

# Getting input from a user
# name = input("Enter your name: ")
# age = input("Enter your age: ")
# print("Hello " + name + "! You are " + age)

# Building a Basic Calculator
# num1 = input("Enter a number: ")
# num2 = input("Enter another number: ")
# result = int(num1) + int(num2)
# int - interger. The data type of the num1 and num2 are in string as default. We need to convert them to numbers. It is a whole number and it can't have a decimal point
# result = float(num1) + float(num2)
# float - a function that can have decimal numbers
# print(result)

# Mad Libs Game
# color = input("Enter a color: ")
# plural_noun = input("Enter a plural noun: ")
# celebrity = input("Enter a celebrity: ")

# print("Roses are " + color)
# print(plural_noun + " are blue")
# print("I love " + celebrity)

# Lists
friends = ["Kevin", "Mary", "John", "Denver", "Susan"]
print(friends)

# index start with 0 (left to right)
print(friends[0])

# it can also start from right to left by counting from -1
print(friends[-1])

# by putting an index and colon you exclude preceding values
print(friends[2:])

# by putting an index, colon, and another index you call out the values within the called index except to the last index.
print(friends[1:4])

# we can modify the value of the list by using the variable below
friends[3] = "Mike"
print(friends[3])